export const CONF_LOCAL = {
  production: false,
  environment: 'LOCAL',
  api: 'http://localhost:4242',
};
